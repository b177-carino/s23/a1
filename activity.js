// insert a single room (insertOne method)

db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
})

// Insert multiple rooms (insertMany method)
db.rooms.insertMany([{
	name: "double",
	accommodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
},
{
	name: "queen",
	accommodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false
}

])

// Use the find method to search for a room with the name double.
db.rooms.findOne({
	name: "double"
})

// Use the updateOne method to update the queen room and set the available rooms to 0.

db.rooms.updateOne(
	// or {name: queen}
	{_id : ObjectId("62877d07f2a1d310d938d346")},
	{
	$set: {
		rooms_available: 0
	}
	}
)

db.rooms.deleteMany({
	rooms_available: 0
})